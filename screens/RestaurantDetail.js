import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import About from '../components/restaurantDetail/About';
import MenuItems from '../components/restaurantDetail/MenuItems';
import ViewCart from '../components/restaurantDetail/ViewCart';
import { Divider } from 'react-native-elements';

const foods = [
    {
        title: 'Lasagna',
        description: 'With butter lettuce, tomato and sauce bechamel 🥧🍷',
        price: '$13.50',
        image: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F19%2F2013%2F01%2F22%2Fthree-cheese-lasagna-ck-2000.jpg&q=60',
    },
    {
        title: 'Tandoori Chicken',
        description: 'Amazin food dish with tenderloin chicken 🥗🍗',
        price: '$19.20',
        image: 'https://cdn.shopify.com/s/files/1/0148/1945/9126/articles/Tandoori_Chicken_5f5e8a4b-c5e0-4df5-91bd-46b1ee6a1bc5_1215x.jpg?v=1606237398',
    },
    {
        title: 'Chilaquiles',
        description: 'Chilaquiles with cheese & sauce 🌮🇲🇽',
        price: '$16.50',
        image: 'https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/06/chilaquilesrojosbajosengrasa.jpg',
    },
    {
        title: 'Wings Chicken and More',
        description: 'Wings with sauce and so much 🍟🥤',
        price: '$26.99',
        image: 'https://resizer.otstatic.com/v2/photos/wide-huge/1/30459428.jpg',
    },
    // {
    //     title: 'Wings Chicken',
    //     description: 'Wings with sauce and so much 🍟🥤',
    //     price: '$26.99',
    //     image: 'https://resizer.otstatic.com/v2/photos/wide-huge/1/30459428.jpg',
    // },
];

const RestaurantDetail = ({ route, navigation }) => {
    return (
        <ScrollView>
            <View>
                <About route={route} />
                <Divider width={1.8} style={{ marginVertical: 20 }} />
                <MenuItems restaurantName={route.params.name} foods={foods} />
                <Divider width={50} style={{ marginTop: 15 }} />
                <ViewCart navigation={navigation} />
            </View>
        </ScrollView>
    );
};

export default RestaurantDetail;
