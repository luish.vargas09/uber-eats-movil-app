import { Image, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import LoginForm from '../components/loginScreen/LoginForm';

const Login = ({ navigation }) => (
    <View style={styles.container}>
        <View style={styles.logoContainer}>
            <Image
                source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/1886/1886770.png',
                    width: 150,
                    height: 150,
                }}
            />
            <Text style={styles.loginText}>BIENVENIDO</Text>
        </View>
        <LoginForm navigation={navigation} />
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 50,
        paddingHorizontal: 12,
    },

    logoContainer: {
        alignItems: 'center',
        marginTop: 60,
    },

    loginText: {
        fontSize: 30,
        fontWeight: 'bold',
    },
});

export default Login;
