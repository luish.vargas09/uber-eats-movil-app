import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    StatusBar,
    ScrollView,
} from 'react-native';
import { useSelector } from 'react-redux';
import LottieView from 'lottie-react-native';
import MenuItems from '../components/restaurantDetail/MenuItems';
import { firebase, db } from '../firebase';
import Photo from '../components/home/Photo';

const OrderCompleted = ({ navigation }) => {
    const [lastOrder, setLastOrder] = useState({
        items: [
            {
                title: 'Lasagna',
                description:
                    'With butter lettuce, tomato and sauce bechamel 🥧🍷',
                price: '$13.50',
                image: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F19%2F2013%2F01%2F22%2Fthree-cheese-lasagna-ck-2000.jpg&q=60',
            },
        ],
    });
    const { items, restaurantName } = useSelector(
        (state) => state.cartReducer.selectedItems
    );

    const total = items
        .map((item) => Number(item.price.replace('$', '')))
        .reduce((previous, current) => previous + current, 0);

    const totalUSD = total.toPrecision(4);
    console.log(totalUSD);

    useEffect(() => {
        const unsubscribe = db
            .collection('orders')
            .orderBy('createdAt', 'desc')
            .limit(1)
            .onSnapshot((snapshot) => {
                snapshot.docs.map((doc) => {
                    setLastOrder(doc.data());
                });
            });
        return () => unsubscribe();
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <View
                style={{
                    margin: 15,
                    alignItems: 'baseline',
                    height: '100%',
                }}
            >
                <LottieView
                    style={{
                        height: 100,
                        alignSelf: 'center',
                        marginBottom: 30,
                    }}
                    source={require('../assets/animations/check-mark.json')}
                    autoPlay
                    speed={0.5}
                    loop={false}
                />
                <Text
                    style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginBottom: 20,
                        marginLeft: 15,
                    }}
                >
                    Your order at {restaurantName} Restaurant has been placed
                    for ${totalUSD} USD <Photo navigation={navigation} />
                </Text>
                <ScrollView>
                    <MenuItems foods={lastOrder.items} hideCheckbox={true} />
                </ScrollView>
                <LottieView
                    style={{
                        height: 200,
                        alignSelf: 'center',
                        marginBottom: 50,
                    }}
                    source={require('../assets/animations/cooking.json')}
                    autoPlay
                />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight,
        // padding: 10,
        backgroundColor: 'white',
    },
});

export default OrderCompleted;
