import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { Divider } from 'react-native-elements';
import Categories from '../components/home/Categories';
import HeaderTabs from '../components/home/HeaderTabs';
import BottomTabs from '../components/home/BottomTabs';
import RestaurantItems, {
    localRestaurants,
} from '../components/home/RestaurantItems';
import SearchBar from '../components/home/SearchBar';
import Photo from '../components/home/Photo';

const YELP_API_KEY =
    'GDCk7Pcvp_Vs1BGvwU4P0TDxmOqKJugL8jB-KLGvggCG3FTaatFRRvZJqUAAwJTvbHt5rtvB9UAkdwEL_zx5YoVNK0Z6WWAug0AQmDgzDskuDHCshaRw5HZtXVB1Y3Yx';

const Home = ({ navigation }) => {
    const [restaurantData, setRestaurantData] = useState(localRestaurants);
    const [city, setCity] = useState('Phoenix');
    const [activeTab, setActiveTab] = useState('Delivery');

    const getRestaurantsFromYelp = () => {
        const yelpUrl = `https://api.yelp.com/v3/businesses/search?term=restaurants&location=${city}`;
        const apiOptions = {
            headers: {
                Authorization: `Bearer ${YELP_API_KEY}`,
            },
        };

        const data = fetch(yelpUrl, apiOptions)
            .then((res) => res.json())
            .then((data) => {
                setRestaurantData(data.businesses);
                // setRestaurantData(
                //     data.businesses.filter((business) =>
                //         business.transactions.includes(activeTab.toLowerCase())
                //     )
                // );
            });
        return data;
    };

    useEffect(() => {
        getRestaurantsFromYelp();
    }, [city, activeTab]);

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ backgroundColor: 'white', padding: 15 }}>
                <HeaderTabs activeTab={activeTab} setActiveTab={setActiveTab} />
                <SearchBar setCity={setCity} />
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Categories />
                <RestaurantItems
                    restaurantData={restaurantData}
                    navigation={navigation}
                />
            </ScrollView>
            <Divider width={1} />
            <BottomTabs navigation={navigation} />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight,
        // padding: 10,
        backgroundColor: '#eee',
    },
});

export default Home;
