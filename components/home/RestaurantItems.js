import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const localRestaurants = [
    {
        name: 'Kitchen Thai Restaurant',
        image_url:
            'https://cdn.pixabay.com/photo/2022/02/10/05/45/lantern-7004643_960_720.jpg',
        categories: ['Cafe', 'Bar'],
        price: '$$',
        reviews: 12377,
        rating: 4.7,
    },
    {
        name: 'Beachside Bar',
        image_url:
            'https://cdn.pixabay.com/photo/2015/09/28/14/20/sunset-962156_960_720.jpg',
        categories: ['Cafe', 'Bar'],
        price: '$$',
        reviews: 9489,
        rating: 4.9,
    },
    {
        name: 'Mexican Grill',
        image_url:
            'https://cdn.pixabay.com/photo/2021/01/19/23/16/tacos-5932654_960_720.jpg',
        categories: ['Indian', 'Bar'],
        price: '$$',
        reviews: 6835,
        rating: 4.6,
    },
    {
        name: 'Italian Grill',
        image_url:
            'https://cdn.pixabay.com/photo/2014/04/22/02/56/pizza-329523_960_720.jpg',
        categories: ['Indian', 'Bar'],
        price: '$$',
        reviews: 16835,
        rating: 4.3,
    },
];

const RestaurantItems = ({ navigation, ...props }) => {
    return (
        <>
            {props.restaurantData.map((restaurant, index) => (
                <TouchableOpacity
                    key={index}
                    activeOpacity={0.6}
                    style={{ marginBottom: 30 }}
                    onPress={() =>
                        navigation.navigate('RestaurantDetail', {
                            name: restaurant.name,
                            image: restaurant.image_url,
                            price: restaurant.price,
                            reviews: restaurant.review_count,
                            rating: restaurant.rating,
                            categories: restaurant.categories,
                        })
                    }
                >
                    <View
                        style={{
                            marginTop: 10,
                            padding: 15,
                            backgroundColor: 'white',
                        }}
                    >
                        <RestaurantImage image_url={restaurant.image_url} />
                        <RestaurantInfo
                            name={restaurant.name}
                            rating={restaurant.rating}
                        />
                    </View>
                </TouchableOpacity>
            ))}
        </>
    );
};

const RestaurantImage = (props) => (
    <>
        <Image
            // source={require('./../assets/images/bg1.jpg')}
            source={{
                uri: props.image_url,
            }}
            style={{ width: '100%', height: 180 }}
        />
        <TouchableOpacity style={{ position: 'absolute', right: 20, top: 20 }}>
            <MaterialCommunityIcons
                name='heart-outline'
                size={25}
                color='#fff'
            />
        </TouchableOpacity>
    </>
);

const RestaurantInfo = (props) => (
    <View
        style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
        }}
    >
        <View>
            <Text style={{ fonstSize: 15, fontWeight: 'bold' }}>
                {props.name}
            </Text>

            <Text style={{ fonstSize: 15, color: 'gray' }}>35 - 45 • min </Text>
        </View>
        <View
            style={{
                backgroundColor: '#eee',
                width: 30,
                height: 30,
                backgroundColor: '#eee',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 15,
            }}
        >
            <Text>{props.rating}</Text>
        </View>
    </View>
);

export default RestaurantItems;
