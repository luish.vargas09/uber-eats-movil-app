import { View, Text } from 'react-native';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Image } from 'react-native';

const Photo = ({ navigation }) => {
    return (
        <TouchableOpacity onPress={() => navigation.push('PhotoCloud')}>
            <Image
                style={{
                    width: 25,
                    height: 25,
                    marginLeft: 60,
                }}
                source={require('../../assets/icons/share.png')}
            />
        </TouchableOpacity>
    );
};

export default Photo;
