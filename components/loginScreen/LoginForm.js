import React, { useState } from 'react-native';
import {
    View,
    Text,
    TextInput,
    Button,
    StyleSheet,
    Pressable,
    TouchableOpacity,
    Alert,
} from 'react-native';

import { Formik } from 'formik';
import * as Yup from 'yup';
import Validator from 'email-validator';

import { firebase, db } from '../../firebase';

const LoginForm = ({ navigation }) => {
    const LoginFormSchema = Yup.object().shape({
        email: Yup.string().email().required('An email is required'),
        password: Yup.string()
            .required()
            .min(8, 'Your password has to have at least 8 characters'),
    });

    const onLogin = async (email, password) => {
        try {
            await firebase.auth().signInWithEmailAndPassword(email, password);
            console.log('🔥 Firebase Login Successful: ✅', email, password);
        } catch (error) {
            Alert.alert(
                '🔥My Lord...',
                error.message + '\n\nWhat would you like to do next?',
                [
                    {
                        text: 'Try Again',
                        onPress: () => console.log('OK'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sign Up',
                        onPress: () => navigation.push('SignUp'),
                    },
                ]
            );
        }
    };

    return (
        <View style={styles.wrapper}>
            <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={(values) => onLogin(values.email, values.password)}
                validationSchema={LoginFormSchema}
                validateOnMount={true}
            >
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    isValid,
                }) => (
                    <>
                        <View
                            style={[
                                styles.inputField,
                                {
                                    borderColor:
                                        values.email.length < 1 ||
                                        Validator.validate(values.email)
                                            ? '#ccc'
                                            : 'red',
                                },
                            ]}
                        >
                            <TextInput
                                placeholderTextColor='#444'
                                placeholder='Email'
                                autoCapitalize='none'
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                autoFocus={true}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                            />
                        </View>

                        <View
                            style={[
                                styles.inputField,
                                {
                                    borderColor:
                                        values.password.length < 1 ||
                                        values.password.length >= 8
                                            ? '#ccc'
                                            : 'red',
                                },
                            ]}
                        >
                            <TextInput
                                placeholderTextColor='#444'
                                placeholder='Password'
                                autoCapitalize='none'
                                autoCorrect={false}
                                secureTextEntry={true}
                                textContentType='password'
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                            />
                        </View>

                        <View style={styles.forgotPassword}>
                            <Text style={{ color: '#6BB0F5' }}>
                                Forgot password?
                            </Text>
                        </View>

                        <Pressable
                            titleSize={20}
                            style={styles.button(isValid)}
                            onPress={() => {
                                handleSubmit;
                                navigation.push('Home');
                            }}
                            disabled={!isValid}
                        >
                            <Text style={styles.buttonText}>Log in</Text>
                        </Pressable>

                        <View style={styles.signupContainer}>
                            <Text>Don't have an aacount?</Text>
                            <TouchableOpacity
                                onPress={() => navigation.push('SignUp')}
                            >
                                <Text style={{ color: '#6BB0F5' }}>
                                    {' '}
                                    Sign Up
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </>
                )}
            </Formik>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 40,
    },

    inputField: {
        borderRadius: 10,
        padding: 10,
        backgroundColor: '#FAFAFA',
        marginBottom: 10,
        borderWidth: 1,
    },

    forgotPassword: {
        alignItems: 'flex-end',
        marginBottom: 30,
    },

    button: (isValid) => ({
        backgroundColor: isValid ? '#0096f6' : '#9ACAF7',
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 42,
        borderRadius: 4,
    }),

    buttonText: {
        fontWeight: '600',
        color: '#fff',
        fontSize: 18,
    },

    signupContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginTop: 50,
    },
});

export default LoginForm;
