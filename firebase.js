// Import the functions you need from the SDKs you need
import firebase from 'firebase';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyBYGjqzHaTDRMUbRc3ZSwkTy1kniQ7XJCg',
    authDomain: 'uber-eats-clone-c0131.firebaseapp.com',
    projectId: 'uber-eats-clone-c0131',
    storageBucket: 'uber-eats-clone-c0131.appspot.com',
    messagingSenderId: '965186655402',
    appId: '1:965186655402:web:696a316880e0ea079eecd3',
};

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

const db = firebase.firestore();

export { firebase, db };
